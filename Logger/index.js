const schedule = require('node-schedule');
const mongodb = require('./lib/mongo');
const amqpdb = require('./lib/amqp');
const async = require('async');


let scheduler_15m = '0 0,15,30,45 * * * *';
let scheduler_1m = '0 */1 * * * *';

let basedata = [];
let indexs = [];

let lastImport = [];

connect();

schedule.scheduleJob(scheduler_15m, fireDate => {
    loop_15m(fireDate);
});

schedule.scheduleJob(scheduler_1m, fireDate => {
    loop_1m(fireDate);
});

function loop_15m(fireDate) {
    mongodb.read().then(result => {
        result.forEach(element => {
            let index = basedata.findIndex(object => {return object.AnlagenId == element.AnlagenId && object.UnitId == element.UnitId && object.Zaehlpunkt == element.Zählpunkt});
            let eisman = null;

            if(index >= 0) {
                eisman = basedata[index].Eisman;
                basedata.splice(index, 1);
            }
            
            basedata.push({
                "UnitId": element.UnitId,
                "AnlagenId": element.AnlagenId,
                "AnlagenName": element.AnlagenName,
                "AnlagenTyp": element.AnlagenTyp,
                "Profil": element.Profil,
                "Schnittstelle": element.Schnittstelle,
                "Zaehlpunkt": element.Zählpunkt,
                "Breitengrad": element.Breitengrad,
                "Laengengrad": element.Längengrad,
                "Zone": element.Zone,
                "Nennleistung": parseInt(element.Nennleistung),
                "Eisman": eisman
            });
        });

        indexs = [];
        basedata.forEach(element => {
            let index = result.findIndex(object => {return (object.AnlagenId == element.AnlagenId && object.UnitId == element.UnitId && object.Zählpunkt == element.Zaehlpunkt)});

            if(index == -1) {
                indexs.push({
                    "AnlagenId": element.AnlagenId,
                    "UnitId": element.UnitId,
                    "Zaehlpunkt": element.Zaehlpunkt
                });
            }
        });
        indexs.forEach(element => {
            let baseIndex = basedata.findIndex(object => {return object.AnlagenId == element.AnlagenId && object.UnitId == element.UnitId && object.Zählpunkt == element.Zaehlpunkt});
            let lastIndex = lastImport.findIndex(object => {return object.UnitId == element.UnitId && object.Zählpunkt == element.Zaehlpunkt});

            if(baseIndex >= 0) {
                basedata.splice(baseIndex, 1);
            }
            if(lastIndex >= 0) {
                lastImport.splice(lastIndex, 1);
            }
        });
    }).catch(error => {
        console.log(error);
    });
}

function loop_1m(fireDate) {
    let errordata = [];
    let currentTime = new Date().setSeconds(0, 0) / 1000;

    lastImport.forEach(element => {
        let base = basedata.find(object => {return object.UnitId == element.UnitId && object.Zaehlpunkt == element.Zaehlpunkt});
        let diff = currentTime - element.LetzterImport;

        if(diff > 420 && base.Schnittstelle != undefined && base.Schnittstelle != null && base.Schnittstelle != "") {
            errordata.push({
                "UnitId": element.UnitId,
                "Zaehlpunkt": element.Zaehlpunkt,
                "Statuscode": 1,
                "timestamp": currentTime
            });
        }
    });
    
    let errorJSON = errorData(errordata);
    amqpdb.publishInflux(errorJSON).catch((error) => {
        console.log(error);
    });
}

function loopInflux(msq) {
    //Daten per AMQP holen
    let livedata = JSON.parse(msq.content.toString());
    let data = [];
    let influxdata = [];
    let errordata = [];
    let debug = false;
    let currentTime = new Date().getTime() / 1000;
    
    livedata.forEach(element => {
        if(element == null) { return; }

        if(debug) {
            if(element.tags.AnlagenId == 1214 || element.tags.AnlagenId == 1224 || element.tags.AnlagenId == 1225) {
                console.log(currentTime + "- Daten aus RabbitMQ:");
                console.log(element);
            }
        }

        let base = [];
        let Nennleistung = 0;

        basedata.forEach(eBasedata => {
            if(eBasedata.AnlagenId == element.tags.AnlagenId) {
                base.push({
                    "AnlagenId": eBasedata.AnlagenId,
                    "UnitId": eBasedata.UnitId,
                    "Zaehlpunkt": eBasedata.Zaehlpunkt,
                    "Nennleistung": eBasedata.Nennleistung
                });
                Nennleistung += eBasedata.Nennleistung;
            }
        });

        base.forEach(eBase => {
            if(debug) {
                if(eBase.AnlagenId == 1214 || eBase.AnlagenId == 1224 || eBase.AnlagenId == 1225) {
                    console.log(currentTime + "- Daten aus Mongo:");
                    console.log(eBase);
                }
            }

            let tmp = data.find(object => {return object.tags.UnitId == eBase.UnitId && object.tags.Zaehlpunkt == eBase.Zaehlpunkt});
            let Verbindung = true;
            let plusIstleistung1 = false;
            let plusIstleistung2 = false;
        
            if(element.fields.Verbindung != undefined && element.fields.Verbindung != null && !isNaN(element.fields.Verbindung)) {
                Verbindung = element.fields.Verbindung;
            }

            if(tmp != undefined && tmp.tags.UnitId == "") {
                if(element.fields.Eingefroren) {
                    tmp.fields.TEingefroren = true;
                } else {
                    if(tmp.fields.Eingefroren) {
                        tmp.fields.Istleistung = 0;
                    }

                    plusIstleistung1 = true;
                    tmp.fields.Eingefroren = false;
                }
                
                if(!Verbindung) {
                    tmp.fields.TAbgebrochen = true;
                } else {
                    if(!tmp.fields.Verbindung) {
                        tmp.fields.Istleistung = 0;
                    }

                    plusIstleistung2 = true;
                    tmp.fields.Verbindung = true;
                }

                if(plusIstleistung1 && plusIstleistung2) {
                    tmp.fields.Istleistung += element.fields.Istleistung;
                }
            } else {
                let Istleistung = element.fields.Istleistung * (eBase.Nennleistung / Nennleistung);

                data.push({
                    "measurement": element.measurement,
                    "fields": {
                        "Istleistung": Istleistung,
                        "Windstaerke": element.fields.Windstaerke,
                        "Eisman": element.fields.Eisman,
                        "Eingefroren": element.fields.Eingefroren,
                        "TEingefroren": false,
                        "Verbindung": Verbindung,
                        "TAbgebrochen": false
                    },
                    "tags": {
                        "AnlagenId": eBase.AnlagenId,
                        "UnitId": eBase.UnitId,
                        "Zaehlpunkt": eBase.Zaehlpunkt,
                        "Nennleistung": Nennleistung
                    },
                    "timestamp": element.timestamp
                });
            }
        });
    });

    data.forEach(element => {
        if(debug) {
            if(element.tags.AnlagenId == 1214 || element.tags.AnlagenId == 1224 || element.tags.AnlagenId == 1225) {
                console.log(currentTime + "- Daten gerechnet:");
                console.log(element);
            }
        }

        let last = lastImport.find(object => {return object.UnitId == element.tags.UnitId && object.Zaehlpunkt == element.tags.Zaehlpunkt});
        let error = false;

        //Abarbeitung nur bei gültiger Istleistung und Verbindung
        if(element.fields.Istleistung != undefined && element.fields.Istleistung != null && !isNaN(element.fields.Istleistung) && element.fields.Verbindung) {
            //Daten auf Eingefroren und Plausibilität prüfen
            if(element.fields.Eingefroren) {
                error = true;
                errordata.push({
                    "UnitId": element.tags.UnitId,
                    "Zaehlpunkt": element.tags.Zaehlpunkt,
                    "Statuscode": 2,
                    "timestamp": element.timestamp
                });
            } else if(element.fields.Istleistung > element.tags.Nennleistung * 1.2) {
                error = true;
                errordata.push({
                    "UnitId": element.tags.UnitId,
                    "Zaehlpunkt": element.tags.Zaehlpunkt,
                    "Statuscode": 3,
                    "timestamp": element.timestamp
                });
            } else if(element.fields.Istleistung < -1000) {
                error = true;
                errordata.push({
                    "UnitId": element.tags.UnitId,
                    "Zaehlpunkt": element.tags.Zaehlpunkt,
                    "Statuscode": 4,
                    "timestamp": element.timestamp
                });
            } else {
                let statusCode = 0;

                if(element.fields.TEingefroren) {
                    statusCode = 5;
                }
                if(element.fields.TAbgebrochen) {
                    statusCode = 6;
                }

                influxdata.push({
                    "UnitId": element.tags.UnitId,
                    "Zaehlpunkt": element.tags.Zaehlpunkt,
                    "Istleistung": parseFloat(element.fields.Istleistung),
                    "Windstaerke": parseFloat(element.fields.Windstaerke),
                    "Eisman": parseInt(element.fields.Eisman),
                    "Statuscode": statusCode,
                    "timestamp": element.timestamp
                });

                //Letzter Wert und Import setzen
                if(last != undefined) {
                    last.LetzterWert = element.timestamp;
                    last.LetzterImport = element.timestamp;
                } else {
                    lastImport.push({
                        "UnitId": element.tags.UnitId,
                        "Zaehlpunkt": element.tags.Zaehlpunkt,
                        "LetzterWert": element.timestamp,
                        "LetzterImport": element.timestamp
                    });
                }
            }

            //Letzter Import setzen
            if(error) {
                if(last != undefined) {
                    last.LetzterImport = element.timestamp;
                } else {
                    lastImport.push({
                        "UnitId": element.tags.UnitId,
                        "Zaehlpunkt": element.tags.Zaehlpunkt,
                        "LetzterWert": 0,
                        "LetzterImport": element.timestamp
                    });
                }
            }
        }
    });

    let influxJSON = influxData(influxdata);
    let errorJSON = errorData(errordata);

    async.parallel([
        function(callback) {
            //influxdata per AMQP senden
            amqpdb.publishInflux(influxJSON).then(() => {
                callback();
            }).catch((error) => {
                callback(error);
            });
        },
        function(callback) {
            //errordata per AMQP senden
            amqpdb.publishInflux(errorJSON).then(() => {
                callback();
            }).catch((error) => {
                callback(error);
            });
        },
        function(callback) {
            //lastImport per AMQP senden
            amqpdb.publishLast(lastImport).then(() => {
                callback();
            }).catch((error) => {
                callback(error);
            });
        }
    ], (error, results) => {
        //ACK für obiges Daten holen per AMQP senden
        if(error) {
            amqpdb.nack(msq);
        } else {
            amqpdb.ack(msq);
        }
    });
}

function loopLast(msq) {
    //Daten per AMQP holen
    let lastdata = JSON.parse(msq.content.toString());

    lastdata.forEach(element => {
        if(element.NeuerLogger != undefined) {
            //lastImport per AMQP senden
            amqpdb.publishLast(lastImport).catch((error) => {
                console.log(error);
            });
        } else if(element.EinsMan != undefined) {
            let base = basedata.find(object => {return object.UnitId == element.UnitId && object.Zaehlpunkt == element.Zaehlpunkt});

            if(base != undefined) {
                base.Eisman = element.EinsMan;
            }
        } else {
            let last = lastImport.find(object => {return object.UnitId == element.UnitId && object.Zaehlpunkt == element.Zaehlpunkt});
    
            //Letzter Wert und Import aktualisieren
            if(last != undefined) {
                if(element.LetzterWert > last.LetzterWert) {
                    last.LetzterWert = element.LetzterWert;
                }
                if(element.LetzterImport > last.LetzterImport) {
                    last.LetzterImport = element.LetzterImport;
                }
            } else {
                lastImport.push({
                    "UnitId": element.UnitId,
                    "Zaehlpunkt": element.Zaehlpunkt,
                    "LetzterWert": element.LetzterWert,
                    "LetzterImport": element.LetzterImport
                });
            }
        }
    });
}

function influxData(influxdata) {
    let influxJSON = [];

    influxdata.forEach(element => {
        let base = basedata.find(object => {return object.UnitId == element.UnitId && object.Zaehlpunkt == element.Zaehlpunkt});
        let last = lastImport.find(object => {return object.UnitId == element.UnitId && object.Zaehlpunkt == element.Zaehlpunkt});
        if(base == undefined) { return; }

        let Windstaerke
        if(element.Windstaerke != undefined && element.Windstaerke != null && !isNaN(element.Windstaerke)) {
            Windstaerke = element.Windstaerke;
        } else {
            Windstaerke = 0;
        }

        let Eisman = 0;
        if(element.Eisman != undefined && element.Eisman != null && !isNaN(element.Eisman)) {
            Eisman = element.Eisman;
        } else {
            if(base.Eisman != null) {
                Eisman = base.Eisman;
            }
        }

        let Schnittstelle = base.Schnittstelle;
        if(!Schnittstelle.includes('PV_')) {
            Schnittstelle = base.Schnittstelle.split('_')[0];
        }
        if(Schnittstelle.includes('SAM')) {
            Schnittstelle = 'Scada and More'
        } else if(Schnittstelle.includes('SMA')) {
            Schnittstelle = 'SMA'
        }

        influxJSON.push({
            "measurement": "pow_data_all",
            "fields": {
                "Nennleistung": parseInt(base.Nennleistung),
                "Istleistung": parseFloat(element.Istleistung),
                "Eisman": parseInt(Eisman),
                "Windstaerke": parseFloat(Windstaerke),
                "Statuscode": element.Statuscode,
                "LetzterImport": new Date(new Date(last.LetzterWert * 1000).setSeconds(0, 0))
            },
            "tags": {
                "UnitId": base.UnitId || null,
                "AnlagenName": base.AnlagenName,
                "AnlagenTyp": base.AnlagenTyp,
                "Profil": base.Profil,
                "Schnittstelle": Schnittstelle,
                "Zaehlpunkt": base.Zaehlpunkt,
                "Breitengrad": base.Breitengrad,
                "Laengengrad": base.Laengengrad,
                "Zone": base.Zone || null
            },
            "timestamp": new Date(element.timestamp * 1000).setSeconds(0, 0) / 1000
        });
    });

    return influxJSON;
}

function errorData(errordata) {
    let errorJSON = [];

    errordata.forEach(element => {
        let base = basedata.find(object => {return object.UnitId == element.UnitId && object.Zaehlpunkt == element.Zaehlpunkt});
        let last = lastImport.find(object => {return object.UnitId == element.UnitId && object.Zaehlpunkt == element.Zaehlpunkt});
        if(base == undefined) { return; }

        let Schnittstelle = base.Schnittstelle;
        if(!Schnittstelle.includes('PV_')) {
            Schnittstelle = base.Schnittstelle.split('_')[0];
        }
        if(Schnittstelle.includes('SAM')) {
            Schnittstelle = 'Scada and More'
        } else if(Schnittstelle.includes('SMA')) {
            Schnittstelle = 'SMA'
        }

        errorJSON.push({
            "measurement": "pow_data_all",
            "fields": {
                "Nennleistung": parseInt(base.Nennleistung),
                "Eisman": 0,
                "Statuscode": element.Statuscode,
                "LetzterImport": new Date(new Date(last.LetzterWert * 1000).setSeconds(0, 0))
            },
            "tags": {
                "UnitId": base.UnitId || null,
                "AnlagenName": base.AnlagenName,
                "AnlagenTyp": base.AnlagenTyp,
                "Profil": base.Profil,
                "Schnittstelle": Schnittstelle,
                "Zaehlpunkt": base.Zaehlpunkt,
                "Breitengrad": base.Breitengrad,
                "Laengengrad": base.Laengengrad,
                "Zone": base.Zone || null
            },
            "timestamp": new Date(element.timestamp * 1000).setSeconds(0, 0) / 1000
        });
    });

    return errorJSON;
}


amqpdb.on('subInflux', loopInflux);
amqpdb.on('subLast', loopLast);
amqpdb.on('connect', () => {setTimeout(() => {connect()}, 5000)});
amqpdb.on('createChannel', () => {setTimeout(() => {createChannel()}, 5000)});


function connect() {
    loop_15m(Date.now());

    amqpdb.connect().then(() => {
        createChannel();
    }).catch(error => {
        console.log(error);

        setTimeout(() => {connect()}, 5000);
    });
}

function createChannel() {
    console.log("Start: " + new Date());

    amqpdb.createChannel().then(() => {
        subscribeLast();

        setTimeout(() => {subscribeInflux()}, 5000);
    }).catch((error) => {
        console.log(error);

        if(error != "createChannel: Keine Verbindung zu RabbitMQ") {
            setTimeout(() => {createChannel()}, 5000);
        }
    });
}

function subscribeLast() {
    amqpdb.subscribelast().then(() => {
        amqpdb.publishLast([{"NeuerLogger": "OK"}]).catch((error) => {
            console.log(error);
        });
    }).catch((error) => {
        console.log(error);

        if(error != "subscribeLast: Keine Verbindung zu RabbitMQ" && error != "subscribeLast: Kein Channel zu RabbitMQ") {
            setTimeout(() => {subscribeLast()}, 5000);
        }
    });
}

function subscribeInflux() {
    let currentTime = new Date().setSeconds(0, 0) / 1000;

    basedata.forEach(element => {
        let last = lastImport.find(object => {return object.UnitId == element.UnitId && object.Zaehlpunkt == element.Zaehlpunkt});

        if(last == undefined) {
            lastImport.push({
                "UnitId": element.UnitId,
                "Zaehlpunkt": element.Zaehlpunkt,
                "LetzterWert": currentTime,
                "LetzterImport": currentTime
            });
        }
    });

    amqpdb.subscribeInflux().catch((error) => {
        console.log(error);

        if(error != "subscribeInflux: Keine Verbindung zu RabbitMQ" && error != "subscribeInflux: Kein Channel zu RabbitMQ") {
            setTimeout(() => {subscribeInflux()}, 5000);
        }
    });
}
