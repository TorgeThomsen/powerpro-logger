const amqp = require('amqplib/callback_api');
const events = require('events');

class amqpdb extends events {
    constructor() {
        super();
        this.conn = null;
        this.chan = null;
        this.queue = null;
    }

    connect() {
        return new Promise((resolve, reject) => {
            amqp.connect({protocol: 'amqp',hostname: '10.20.20.81',port: 5672,username: 'logger',password: 'k4QyLdMk',vhost: 'powerpro'}, (error, conn) => {
                if(error) return reject(error);

                this.conn = conn;

                this.conn.on('close', () => {
                    this.conn = null;
                    this.chan = null;
                    this.queue = null;
                    this.emit('connect');
                });

                this.conn.on('error', (error) => {
                    this.conn = null;
                    this.chan = null;
                    this.queue = null;
                    console.log(error);
                });

                resolve(true);
            });
        });
    }

    createChannel() {
        return new Promise((resolve, reject) => {
            if(this.conn != null) {
                this.conn.createChannel((error, ch) => {
                    if(error) return reject(error);

                    this.chan = ch;
        
                    this.chan.on('close', () => {
                        this.chan = null;
                        this.emit('createChannel');
                    });
        
                    this.chan.on('error', (error) => {
                        this.chan = null;
                        console.log(error);
                    });
        
                    resolve(true);
                });
            } else {
                reject("createChannel: Keine Verbindung zu RabbitMQ")
            }
        });
    }

    ack(msq) {
        this.chan.ack(msq);
    }

    nack(msq) {
        this.chan.nack(msq, false, true);
    }

    subscribeInflux() {
        return new Promise((resolve, reject) => {
            if(this.conn != null) {
                if(this.chan != null) {
                    this.chan.prefetch(1);

                    this.chan.assertQueue('pow-data-influx', {durable: true}, (error, q) => {
                        if(error) return reject(error);

                        this.chan.consume(q.queue, msq => {
                            this.emit('subInflux', msq);
                        }, {noAck: false});

                        resolve(true);
                    });
                } else {
                    reject("subscribeInflux: Kein Channel zu RabbitMQ");
                }
            } else {
                reject("subscribeInflux: Keine Verbindung zu RabbitMQ");
            }
        });
    }

    subscribelast() {
        return new Promise((resolve, reject) => {
            if(this.conn != null) {
                if(this.chan != null) {
                    if(this.queue != null) {
                        this.chan.deleteQueue(this.queue);
                        this.queue = null;
                    }

                    this.chan.assertQueue('', {exclusive: true}, (error, q) => {
                        if(error) return reject(error);

                        this.queue = q.queue;

                        this.chan.assertExchange('powLogger', 'fanout', {durable: true}, () => {
                            this.chan.bindQueue(q.queue, 'powLogger', '');

                            this.chan.consume(q.queue, msq => {
                                this.emit('subLast', msq);
                            }, {noAck: true});
    
                            resolve(true);
                        });
                    });
                } else {
                    reject("subscribeLast: Kein Channel zu RabbitMQ");
                }
            } else {
                reject("subscribeLast: Keine Verbindung zu RabbitMQ");
            }
        });
    }

    publishInflux(msq) {
        return new Promise((resolve, reject) => {
            if(this.conn != null) {
                if(this.chan != null) {
                    this.chan.sendToQueue('pow-influxdb', new Buffer(JSON.stringify(msq)));

                    resolve(true);
                } else {
                    reject("publishInflux: Kein Channel zu RabbitMQ");
                }
            } else {
                reject("publishInflux: Keine Verbindung zu RabbitMQ");
            }
        });
    }

    publishLast(msq) {
        return new Promise((resolve, reject) => {
            if(this.conn != null) {
                if(this.chan != null) {
                    this.chan.publish('powLogger', '', new Buffer(JSON.stringify(msq)));

                    resolve(true);
                } else {
                    reject("publishLast: Kein Channel zu RabbitMQ");
                }
            } else {
                reject("publishLast: Keine Verbindung zu RabbitMQ");
            }
        });
    }
}

module.exports = new amqpdb();
