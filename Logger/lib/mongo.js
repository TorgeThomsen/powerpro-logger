const mongo = require('mongodb').MongoClient;
const objectId = require('mongodb').ObjectID;

class mongodb {
    constructor() {
    }

    read() {
        return new Promise(function(resolve, reject) {
            mongo.connect('mongodb://north-tec:X9n9FqI7smzXTh@10.20.20.83:27017/', {useNewUrlParser: true}, (err, client) => {
                if(err) return reject(err);
    
                let db = client.db('NGDataStore');
    
                db.collection('Assets').aggregate(
                    [
                        { 
                            "$match" : {
                                "Eigenschaften.physikalisch.Zählpunkt" : {
                                    "$exists" : true
                                }
                            }
                        }, 
                        { 
                            "$unwind" : {
                                "path" : "$Eigenschaften.allgemein.ID"
                            }
                        }, 
                        { 
                            "$group" : {
                                "_id" : {
                                    "AnlagenId" : "$Eigenschaften.allgemein.ID", 
                                    "Zählpunkt" : "$Eigenschaften.physikalisch.Zählpunkt"
                                },
                                "AnlagenId" : {
                                  "$first" : "$Eigenschaften.allgemein.ID"
                                },
                                "Zählpunkt" : {
                                  "$first" : "$Eigenschaften.physikalisch.Zählpunkt"
                                },
                                "AnlagenName" : {
                                    "$first" : "$Bezeichnung"
                                }, 
                                "AnlagenTyp" : {
                                    "$first" : "$Eigenschaften.physikalisch.Type"
                                }, 
                                "Profil" : {
                                    "$first" : "$Eigenschaften.wirtschaftlich.Profil"
                                }, 
                                "Schnittstelle" : {
                                    "$first" : "$Eigenschaften.physikalisch.Schnittstelle"
                                }, 
                                "Breitengrad" : {
                                    "$first" : "$Eigenschaften.allgemein.Breitengrad"
                                }, 
                                "Längengrad" : {
                                    "$first" : "$Eigenschaften.allgemein.Längengrad"
                                }, 
                                "Zone" : {
                                    "$first" : "$Eigenschaften.allgemein.Zone"
                                }, 
                                "Nennleistung" : {
                                    "$first" : "$Eigenschaften.physikalisch.Nennleistung"
                                }, 
                                "UnitId" : {
                                    "$first" : "$Eigenschaften.physikalisch.Unit ID"
                                }
                            }
                        }
                    ], 
                    { 
                        "allowDiskUse" : false
                    }
                ).toArray((err, result) => {
                    if(err) return reject(err);

                    console.log(result.length + ' mongo entries...');
                    client.close;

                    resolve(result);
                });
/*
                db.collection('Assets').find({'_parent_id': objectId('5ad84fd2c46b74293c2fa173')}).toArray((err, result) => {
                    if(err) return reject(err);

                    console.log(result.length + ' mongo entries...');
                    client.close;

                    resolve(result);
                });
*/
            });
        });
    }
}

module.exports = new mongodb();
