const schedule = require('node-schedule');
const influx = require('influx');


let scheduler_1m = '0 */1 * * * *';


let influxClient = new influx.InfluxDB({
    host: '10.20.20.82',
    port: 8086,
    protocol: 'http',
    username: 'logger',
    password: 'Vc79pUZ5',
    database: 'powerpro'
});


schedule.scheduleJob(scheduler_1m, fireDate => {
    loop_1m(fireDate);
});


function loop_1m(fireDate) {
    let influx = [];

    influxClient.query("SELECT AnlagenName, Nennleistung, Profil, Zaehlpunkt FROM (SELECT AnlagenName, Nennleistung, Profil, Zaehlpunkt, Statuscode FROM pow_data_all WHERE time > now()-3m AND AnlagenTyp != 'BGA') WHERE Statuscode != 0").then(results => {
        let counter = results.length;

        results.forEach(element => {
            let time = parseInt(element.time.getNanoTime()) + 900000000000;
            let query = "SELECT Leistung, Forecaster FROM pow_data_forecast_a WHERE Zaehlernummer = '" + element.Zaehlpunkt + "' AND time >= " + element.time.getNanoTime() + " AND time < " + time;
    
            influxClient.query(query).then(results => {
                results.forEach(forecast => {
                    influx.push({
                        "measurement": "pow_data_forecast_dc",
                        "fields": {
                            "Nennleistung": element.Nennleistung,
                            "Leistung": forecast.Leistung
                        },
                        "tags": {
                            "Profil": element.Profil,
                            "Zaehlpunkt": element.Zaehlpunkt,
                            "Forecaster": forecast.Forecaster
                        },
                        "timestamp": element.time.getNanoTime() / 1000000000
                    });
                });
                if(influx.length < 1) {
                    influx.push({
                        "measurement": "pow_data_forecast_dc",
                        "fields": {
                            "Nennleistung": element.Nennleistung,
                            "Leistung": 0
                        },
                        "tags": {
                            "Profil": element.Profil,
                            "Zaehlpunkt": element.Zaehlpunkt,
                            "Forecaster": "Energyweather"
                        },
                        "timestamp": element.time.getNanoTime() / 1000000000
                    });
                    influx.push({
                        "measurement": "pow_data_forecast_dc",
                        "fields": {
                            "Nennleistung": element.Nennleistung,
                            "Leistung": 0
                        },
                        "tags": {
                            "Profil": element.Profil,
                            "Zaehlpunkt": element.Zaehlpunkt,
                            "Forecaster": "MeteoGroup"
                        },
                        "timestamp": element.time.getNanoTime() / 1000000000
                    });
                } else if(influx.length == 1) {
                    if(influx[0].tags.Forecaster == "MeteoGroup") {
                        influx.push({
                            "measurement": "pow_data_forecast_dc",
                            "fields": {
                                "Nennleistung": element.Nennleistung,
                                "Leistung": 0
                            },
                            "tags": {
                                "Profil": element.Profil,
                                "Zaehlpunkt": element.Zaehlpunkt,
                                "Forecaster": "Energyweather"
                            },
                            "timestamp": element.time.getNanoTime() / 1000000000
                        });
                    } else {
                        influx.push({
                            "measurement": "pow_data_forecast_dc",
                            "fields": {
                                "Nennleistung": element.Nennleistung,
                                "Leistung": 0
                            },
                            "tags": {
                                "Profil": element.Profil,
                                "Zaehlpunkt": element.Zaehlpunkt,
                                "Forecaster": "MeteoGroup"
                            },
                            "timestamp": element.time.getNanoTime() / 1000000000
                        });
                    }
                }
                
                counter--;
                if(counter == 0) {
                    influxClient.writePoints(influx, {precision: 's'});
                }
            }).catch(err => {});
        });
    }).catch(err => {});
}
